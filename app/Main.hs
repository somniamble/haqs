module Main where

import Lib
import System.Environment

main :: IO ()
main = do
  args <- getArgs
  case length args of
    0 -> putStrLn "feed this some text"
    _ -> putStrLn $ show . calculateAQ $ unwords args
