module Lib where

import Data.Char

-- a list of pairs, relating each alphanumeric character to its AQ value
aqValues :: [(Char, Integer)]
aqValues = zip "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ" [0 .. 35]

-- gets the AQ of a character from the aqValues table, or 0 if it is not alphanumeric
getAQ :: Char -> Integer
getAQ = maybe 0 id . (flip lookup aqValues . toUpper)

-- calculates the AQ for every character in a string and sums it all together
calculateAQ :: String -> Integer
calculateAQ = sum . map getAQ
